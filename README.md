## Generate routes file

```
php artisan ziggy:generate "./resources/js/router/routes.js"
```

## Generate localisation file

```
php artisan lang:js --json --quiet
```

## Sitemap

```
php artisan sitemap:generate
```